# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import random 
import time
import sys

#Sciezki do plików
file_help = 'C:/Users/Magda/Documents/Magda_priv/ZR/Gra_text/help.txt'
file_room2 = 'C:/Users/Magda/Documents/Magda_priv/ZR/Gra_text/room2.txt'
file_room1 = 'C:/Users/Magda/Documents/Magda_priv/ZR/Gra_text/room1.txt'

#Stałe globalne
time_room2=300 # czas gry w Room_2 - stała wartosć

n_task_base1 = 13  # całkowita ilosc pytan w bazie (plik tekstowy)
n_task_r1 = 5  # liczba zadań w Room_1 - stała wartosc

n_task_base2 = 13  # całkowita ilosc pytan w bazie (plik tekstowy)
n_task_r2 = 5  # liczba zadań w Room_2 - stała wartosc

# Funkcje
def introduce_room1():
    print_help(file_help, 0)   
    print_help(file_help, 14)
    time.sleep(2)
    print_help(file_help, 1)
    time.sleep(2)

def print_help_r1():   
    print_help(file_help, 1)
    time.sleep(2)  
    print_help(file_help, 14)       

def introduce_room2(): 
    time.sleep(5)         
    print_help(file_help, 4)
    print_help(file_help, 14)
    time.sleep(5)  
    print_help(file_help, 5)
    print_help(file_help, 14)
    time.sleep(2)
    
def select_number(n, k):
    return random.sample(range(n),k) #wybiez k liczb z n liczb
  
def count_period(time_set, start_time, end_time):    
    period=time_set-(end_time-start_time)   
    return period

def print_period(time_set, start_time, end_time):
    period=count_period(time_set, start_time, end_time)
    if period>0:
        print(f"Masz jeszcze {period:.0f} sekund, żeby rozwiazać wszystkie zadania")
    else:
        print("Jeste za wolny, koniec gry")
    
def print_help(file_name, n_line):    
    with open(file_name, 'r') as f:
         lines = f.readlines()
         print(lines[n_line])
                      
def get_line(file_name, n_line):
    with open(file_name, 'r') as f:
         lines = f.readlines()
         return lines[n_line]
    
#Klasy
class Room_1:
    def __init__(self, Id, text, answer_a, answer_b, answer_c, answer_d, truth):
        self.Id = Id
        self.text = text
        self.answer_a = answer_a
        self.answer_b = answer_b
        self.answer_c = answer_c
        self.answer_d = answer_d
        self.truth = truth
          
    def show_task(self):
        time.sleep(2)
        print(self.text)
        time.sleep(3)
        print(f"Odpowiedź a: {self.answer_a}")
        print(f"Odpowiedź b: {self.answer_b}")
        print(f"Odpowiedź c: {self.answer_c}")
        print(f"Odpowiedź d: {self.answer_d}")
        
        print_help(file_help, 2)
        decision = input("Wprowadz: ") 
        if decision=="h":
           print_help_r1() 
               
    def set_answer(self):
        pass
    
    def sumup_room1(self):
        print(f"Prawidlowa odpowiedz to {self.truth}")

class Room_2:
    time_counter=0
    
    def __init__(self, Id, text, your_answer, truth_answer):
        self.Id = Id
        self.text = text
        self.your_answer = your_answer
        self.truth_answer = truth_answer
       
    def show_task(self):    
        time.sleep(2)
        print(self.text)
        print_help(file_help, 16)
        
    def set_answer(self):
        while True:
            try:
                your_answer = int(input("Podaj wynik (liczba całkowita): "))
                self.your_answer = your_answer
                break
            except ValueError:
                print("Nie prawidłowa wartosc, odpowiedzi musi byc liczba")
                         
    def sumup_room2(self):
        print(f"Prawidlowa odpowiedz to {self.truth_answer}")
               
class Life:
    life_counter = 9
    def __init__(self, life_a, life_b, life_c, life_d):
        self.life_a = life_a
        self.life_b = life_b
        self.life_c = life_c
        self.life_d = life_d
        
    def set_life_r1():       
        lives = []
        options = ['a', 'b', 'c', 'd']
        for option in options:
           while True:
            try:
                lives.append(int(input(f"Ile swoich żyć stawiasz na odpowiedź {option}: ")))
                break
            except ValueError:
                print("Nieprawidłowa wartość, odpowiedź musi być liczbą")
        return Life(*lives)
   
    def sum_life_r1(self):
        sum_life_r1=self.life_a+self.life_b+self.life_c+self.life_d
        return sum_life_r1

    def cut_life_r1(self, choice):
        life_options = {"a": self.life_a, "b": self.life_b, "c": self.life_c, "d": self.life_d}
        if choice in life_options:
            #Life.life_counter = self.life_counter = life_options[choice]
            Life.life_counter =  life_options[choice]
        return Life.life_counter

    def cut_life_r2(self):
        Life.life_counter -= 1  # odwołanie do pola statycznego
        self.life_counter = Life.life_counter
        
    def sumup_life(self):
        if Life.life_counter>0:
            print(f"Masz jeszcze {Life.life_counter} żyć.")
        if Life.life_counter==0:
            print("To był zły wybór. Nie masz już żyć")

class Beltlife:
    def __init__(self):
        self.p = 1
        self.c = 1      
        self.f = 1
    
    def set_beltlife(self, decision, file_name, n_line=7): 
        decisions = ["p", "c", "f"]    
        if (decision in decisions) and getattr(self, decision) == 1:
           print_help(file_name, n_line + decisions.index(decision))
           setattr(self, decision, 0)
           Room_2.time_counter = {"p": 120, "c": 60, "f": 30}[decision]
           print_help(file_name, 13)
           time.sleep(Room_2.time_counter)
        elif decision in decisions:
           print("Koło zostało wykorzystane")
        else:
             Room_2.time_counter = 0  # odwołanie do pola statycznego
             self.time_counter = Room_2.time_counter
             print_help(file_name, 14)

def main():
# Jestes w ROOM1
   introduce_room1() 
    
# Losowanie pytan z bazy do room1   
   task_r1=[]
   number_task_r1=select_number(n_task_base1, n_task_r1) 
   #print(number_task_r1)
   for i in number_task_r1:
          task_r1.append(get_line(file_room1, i)) #wybrane pytania z bazy

# Tworzenie pytan (tworzenie instancji klasy Room1)
   for i in range(0, n_task_r1):
       if Life.life_counter > 0:
          task_r1_sel=task_r1[i].split(",")  
          #print(task_r1_sel)
          object_r1=Room_1(*task_r1_sel) # rozpakowanie elementów listy do konstruktora                   
      
          print(f"PYTANIE {i+1} z {n_task_r1}" )
          object_r1.show_task()
          object_life = Life.set_life_r1()
          sum_life = object_life.sum_life_r1()

          # print(Life.life_counter)
          while sum_life != Life.life_counter:
                print(f" Masz {Life.life_counter} żyć a podzieliłes {sum_life}. Zrób to ponownie")
                object_life = Life.set_life_r1()
                sum_life = object_life.sum_life_r1()
                
          Life.life_counter=object_life.cut_life_r1(object_r1.truth.strip())
          #print(object_r1.truth)
          object_r1.sumup_room1()
          object_life.sumup_life()
          
          print_help(file_help, 14)
                  
       if Life.life_counter < 1:
          print_help(file_help, 3)
          sys.exit() 
    
# Przechodzimy do ROOM 2 
   introduce_room2()     # wyswiel info o room2
   start_time=time.time()# ustaw czas startowy     
   #print(start_time)  
   object_beltlife=Beltlife()
            
# Losowanie numerow pytan (bez powtorzen) z bazy  
   task_r2=[]
   number_task_r2=select_number(n_task_base2,n_task_r2) 
    #print(number_quastion_r2)
   for i in number_task_r2:
        task_r2.append(get_line(file_room2, i)) #wybrane pytania z bazy
    #print(list_task)  
     
# Wywolanie pytan  i wprowadzanie odpowiedzi uzytkownika (tworzenie instancji klasy Room2)
   period_1=time_room2
   for i in range(0, n_task_r2):
      
      if Life.life_counter > 0 and period_1>0 :
            task_r2_sel=task_r2[i].split(";")           
            object_r2=Room_2(*task_r2_sel)                   
            
            print(f"ZADANIE {i+1} z {n_task_r2}" )
            object_r2.show_task()
            #print(start_time)  #testowanie
            decision = input("Wprowadz: ") 
            if (decision=="p" or decision=="c"  or decision=="f"):
                object_beltlife.set_beltlife(decision, file_help)
                start_time+=Room_2.time_counter 
           
            #print(start_time)  #testowanie                
            object_r2.set_answer()
                
            if int(object_r2.truth_answer) != int(object_r2.your_answer):
                object_life.cut_life_r2()
        
            object_r2.sumup_room2()
            object_life.sumup_life()
            #end_time2=time.time()
            end_time2=time.time()
            period_1=count_period(time_room2, start_time, end_time2)
            print_period(time_room2, start_time, end_time2)
            print_help(file_help, 14)
            time.sleep(2)
            
      elif period_1<0 :
            print_help(file_help, 10)
            sys.exit() 
            
      elif Life.life_counter<1:
            print_help(file_help, 11)
            sys.exit()
   
   if Life.life_counter > 0 and period_1>0:        
       print_help(file_help, 12)
   else:
       print_help(file_help, 12)
               
if __name__ == "__main__":
    main()
